from mininet.topo import Topo 
from mininet.link import TCLink

class MyTopo( Topo ):  
    "Simple topology example."
    def __init__( self ):
        "Create custom topo."

        # Initialize topology
        Topo.__init__( self )

        # Add hosts and switches
        Host1 = self.addHost( 'h1' )
        Host2 = self.addHost( 'h2' )
        Switch1 = self.addSwitch('s1')

        # Add links
        self.addLink( Host1, Switch1, cls=TCLink, bw=10, delay='10ms', loss=0, max_queue_size=1000, use_htb=True )
        self.addLink( Host2, Switch1, cls=TCLink, bw=10, delay='10ms', loss=0, max_queue_size=1000, use_htb=True )

       

topos = { 'mytopo': ( lambda: MyTopo() ) } 