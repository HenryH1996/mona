

class rate_control_module(object):
	def __init__(self, frame_pieces):

		
		self.frame_pieces = frame_pieces
		self.sending_delay_max = 1 / (60*frame_pieces)
		self.server_sending_delay = self.sending_delay_max

		self.rec_window = 50

		self.cold_start = True
		self.step_1 = 0
		self.utility_1 = 0
		self.step_2 = 0
		self.utility_2 = 0

		self.utility_buffer = []

		self.network_delay_rec = [0,0]   #(min, max)
		self.network_delay_g_rec = [0]*self.rec_window  # stroe previous gradients  
		self.packet_loss_g_rec = [0]*self.rec_window

		self.network_delay_g2_rec = [0]*self.rec_window  # stroe previous gradients  
		self.packet_loss_g2_rec = [0]*self.rec_window

		self.utility_rec = [0]*self.rec_window

		state_list = ['step_1', 'step_2', 'action']
		state = 'step_1'

	def compute_utility(self, monitor_params):

		# monitor_params = {'network_delay':[], 'packet_loss', 'send_queue_delay', 'server_sending_delay'}
		network_delay = monitor_params['network_delay']
		packet_loss = monitor_params["packet_loss"]
		# send_queue_delay = monitor_params["send_queue_delay"]
		# server_sending_delay = monitor_params["server_sending_delay"]


		network_delay_min = min(network_delay) 
		network_delay_max = max(network_delay)
		packet_loss_min = min(packet_loss)
		packet_loss_max = max(packet_loss)

		if(self.cold_start):
			self.network_delay_rec = [network_delay_min, network_delay_max]
			self.packet_loss_rec = [packet_loss_min, packet_loss_max]
			self.cold_start = False
			return

		network_delay_min_g = network_delay_min - self.network_delay_rec[0]
		network_delay_max_g = network_delay_max - self.network_delay_rec[1]

		network_delay_g = 0.7*network_delay_min_g/self.network_delay_rec[0] + 0.3*network_delay_max_g/self.network_delay_rec[1]
		# network_delay_g = 0.7*network_delay_min_g

		self.network_delay_g_rec = self.network_delay_g_rec[1:] + [network_delay_g]

		packet_loss_min_g = packet_loss_min - self.packet_loss_rec[0]
		packet_loss_max_g = packet_loss_max - self.packet_loss_rec[1]
		if(self.packet_loss_rec[0] > 0.001 and self.packet_loss_rec[1] > 0.001):
			packet_loss_g = 0.7*packet_loss_min_g/self.packet_loss_rec[0] + 0.3*packet_loss_max_g/self.packet_loss_rec[1]
		else:
			packet_loss_g = 0.7*packet_loss_min_g + 0.3*packet_loss_max_g

	# packet_loss_g = 0.7 * packet_loss_min_g

		self.packet_loss_g_rec = self.packet_loss_g_rec[1:] + [packet_loss_g]

		self.network_delay_rec = [network_delay_min, network_delay_max]
		self.packet_loss_rec = [packet_loss_min, packet_loss_max]

		self.network_delay_g2_rec = self.network_delay_g2_rec[1:] + [self.network_delay_g_rec[-1] - self.network_delay_g_rec[-2]]
		self.packet_loss_g2_rec = self.packet_loss_g2_rec[1:] + [self.packet_loss_g_rec[-1] - self.packet_loss_g_rec[-2]]

		
		utility = (1 / self.server_sending_delay)/5 - 1000 * network_delay_g - 1000 * packet_loss_g
		self.utility_rec = self.utility_rec[1:] + [utility]

		return 

	def action(self, monitor_params):

		if(state == 'step_1'):
			#increase the sending rate
			state = 'step_2'

			self.server_sending_delay -= 0.0001
			return self.server_sending_delay
		elif(state == 'step_2'):
			#decrease the sending rate
			utility_1 = compute_utility(monitor_params)
			state = 'action'
			return 0
		elif(state == 'action'):
			#take actions
			utility_2 = compute_utility(monitor_params)
			state = 'step_1'

			delta = utility_2 - utility_1

			return 1 if delta<0 else 0 

	def get_non_zero(self, List):
		return [List[i] for i in range(len(List)) if List[i]>0]


