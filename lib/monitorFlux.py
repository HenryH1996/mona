import sys 
import os.path as osp
sys.path.append(osp.abspath(osp.dirname(osp.dirname(__file__))))

from lib.influx_operator import InfluxOP
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import time 
from threading import Thread, Lock
import matplotlib.font_manager as font_manager
from utils.utils import UTCStr_to_LocalDate
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()

class MonitorFlux(object):

	def __init__(self, db_addr, db_auth, db_name, plot_number, monitor_targets=None):

		self.DBoperator = InfluxOP(host=db_addr['ip'], port=db_addr['port'], username=db_auth["username"], password=db_auth['password'])
		self.database = db_name
		if(not self.DBoperator.check_database(self.database)):
			print('create new database: ', self.database)
			self.DBoperator.createDatabase(self.database)

		self.monitor_obj = []

		# used to compute the location of subplots
		self.fig_size = (int(plot_number**(1/2))+1, int(plot_number**(1/2))+1)

		self.fig_count = 0

		self.ax_list = {}
		self.line_list = {}

		self.monitor_targets = monitor_targets

		if(monitor_targets):
			self.monitor = Thread(target=self.initMonitor, args=())
			self.monitor.daemon = True
			self.monitor.start()
			self.plot_ready = False

	def updateMonitor(self):
		print('update plot!!')
		if(not self.plot_ready):
			return
		for obj in self.monitor_obj:
			results = self.DBoperator.pullData(database=self.database, measurement=obj['measurement'], tags=obj['tags'], time_range=obj["time_range"])
			x = [UTCStr_to_LocalDate(data['time']) for data in results]

			y = [data[obj['field']] for data in results]
			self.plot_update_module(obj['field'], x, y)

		plt.draw()


	def initMonitor(self):

		fig = plt.figure(figsize= (10,10))
		for target in self.monitor_targets:
			self.createFigure(target["measurement"], target["field_name"], tags=target['tags'], time_range=target['time_range'], reset_measurement=target['reset'])
		self.plot_ready = True
		plt.show()


	def createFigure(self, measurement, field_name, tags=None, time_range='1m', reset_measurement=False):
		self.fig_count += 1
		self.monitor_obj.append({'measurement': measurement, 'field': field_name, 'tags': tags, 'time_range': time_range})

		fig_loca = str(self.fig_size[0])*2 + str(self.fig_count)
		print('create fig loca: ', int(fig_loca))
		if(reset_measurement):
			self.DBoperator.dropMeasurement(database=self.database, measurement=measurement)

		results = self.DBoperator.pullData(database=self.database, measurement=measurement, tags=tags, time_range=time_range)

		x = [UTCStr_to_LocalDate(data['time']) for data in results]
		y = [data[field_name] for data in results]

		self.plot_initialize_module(loca=int(fig_loca), name=field_name, x=x, y=y, label=field_name)
		

	#initialize the figure
	def plot_initialize_module(self, loca, name, x, y, label):
		self.ax_list[name] = plt.subplot(loca)
		if(len(y)>0):
			self.ax_list[name].set_ylim([0, int(max(y)*2)])
			self.ax_list[name].set_xlim([x[0], x[-1]])
		self.ax_list[name].set_autoscale_on(False)
		self.ax_list[name].grid(True)
		self.ax_list[name].xaxis.set_major_formatter(mdates.DateFormatter("%H:%M:%S"))  
		self.line_list[name], = self.ax_list[name].plot(x, y, label=label, color='cornflowerblue')
		self.ax_list[name].legend(loc='upper center', ncol=4, prop=font_manager.FontProperties(size=10))

	def plot_update_module(self, name, x, y):

		self.line_list[name].set_ydata(y)
		self.line_list[name].set_xdata(x)
		self.ax_list[name].set_xlim([x[0], x[-1]])
		self.ax_list[name].set_ylim(0, max(y)*2)


	def pushData(self, measurement, datapoints, tags=None):

		return self.DBoperator.pushData(self.database, measurement, datapoints, tags)

	def pullData(self, measurement, tags=None, time_range='30m'):

		return self.DBoperator.pullData(self.database, measurement, tags, time_range)

	def deleteData(self, database, measurement, tags=None, time_before='1d'):

		return self.DBoperator.deleteData(database, measurement, tags, time_before)

	def dropMeasurement(self, database, measurement):

		return self.DBoperator.dropMeasurement(database, measurement)

	def dropDatabase(self, database):

		return self.DBoperator.dropDatabase(database)




