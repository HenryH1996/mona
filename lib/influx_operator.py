import sys 
import os.path as osp
sys.path.append(osp.abspath(osp.dirname(osp.dirname(__file__))))
from influxdb import InfluxDBClient
import datetime

class InfluxOP(object):


	def __init__(self, host, port, username, password):

		"""
		input:
			host: ip address of the influxdb service
			port: port of the influxdb service, 8086 in common
			username & password: influxdb authentication params
		"""
		self.client = InfluxDBClient(host=host, port=port, username=username, password=password)

	def createDatabase(self, name):

		"""
		Create a databse if not exist

		input: name: database name
		return: True if success
				False if already existed
		"""
		if(not self.check_database(name)):
			self.client.create_database(name)
			return True
		else:
			print('Database named {} already exists'.format(name))
			return False

	def pushData(self, database, measurement, datapoints, tags=None):
		"""
		Push data piece to the database

		input: database: destination database  
			   measurement: destination sheet
			   datapoints: datapoints' list to push
			   (each data point is a json-style dictionary, all the points should be packed into a list)
			   [{
					
					"field_name_1": field_value_1
					"field_name_2": field_value_2
			   }]
			   tags: tags for the datapoints, default is None
			   {
               		"user": "Carol",
                	"brushId": "6c89f539-71c6-490d-a28d-6c5d84c0ee2f"
         	   }

         	   ***Notice: these datapoints will be assigned with the same time(which is current time).
        output: True if success, else False
		"""
		try:
			if(not self.check_database(database)):
				raise Exception('Database named {} not exist!'.format(database))
			else:
				self.client.switch_database(database)
				formed_datapoints = self.formDataPoints(measurement, datapoints, tags)
				self.client.write_points(formed_datapoints)
				return True
		except Exception as e:
			print('======> Database Error: ',e)
			return False



	def pullData(self, database, measurement, tags=None, time_range='30m'):
		"""
		pull data from the database

		input: database: database name
			   measurement: sheet name
			   tags: tag filter, using python dictionary, example: {'version':'0.3'}
			   time_range: only pull date between now()-time_range and now(), default is '30m'
			   				`ns` nanoseconds  
							`us or µs` microseconds  
							`ms`   milliseconds  
							`s`   seconds     
							`m`   minutes   
							`h` hours   
							`d`  days   
							`w`  weeks  
		ouput: a list of results, every element is a dictionary for one data point									
		"""
		try:
			self.client.switch_database(database)
			results = self.client.query("SELECT * FROM {} WHERE time > now()-{}".format(measurement, time_range))
			if(tags):
				return list(results.get_points(tags=tags))
			else:
				return list(results)
		except Exception as e:
			print('======> Database Error: ',e)
			return []


	def deleteData(self, database, measurement, tags=None, time_before='1d'):

		"""
			Delete expired data 

			input: database: database name
				   measurement: sheet name
				   tags: tag filter, using python dictionary, example: {'version':'0.3'}
				   time_before: only pull date before now()-time_before, default is '1d'
			output: True if success, else Flase
		"""
		try:
			self.client.switch_database(database)
			results = self.client.query("DELETE FROM {} WHERE time < now() - {}".format(measurement, time_before))
			return True
		except Exception as e:
			print('======> Database Error: ',e)
			return False

	def dropMeasurement(self, database, measurement):

		"""
		Delete measurement sheet

		input: database: database name
			   measurement: sheet name
		output: True if success, else Flase
		"""
		try:
			self.client.switch_database(database)
			results = self.client.query("DROP MEASUREMENT {}".format(measurement))
			return True
		except Exception as e:
			print('======> Database Error: ',e)
			return False

	def dropDatabase(self, database):

		"""
		Delete measurement sheet

		input: database: database name
		output: True if success, else Flase
		"""
		try:
			self.client.switch_database(database)
			results = self.client.query("DROP DATABASE {}".format(database))
			return True
		except Exception as e:
			print('======> Database Error: ',e)
			return False

	def check_database(self, name):
		"""
		check whether database exists

		"""
		database_list = self.client.get_list_database()
		for database in database_list:
			if(database['name']==name):
				return True
		return False

	def formDataPoints(self, measurement, datapoints, tags):

		formed_datapoints = []
		for datapoint in datapoints:
			dataPack = {}
			dataPack['time'] = datetime.datetime.utcnow().isoformat()
			dataPack['measurement'] = measurement
			dataPack['fields'] = datapoint
			if(tags is not None):
				dataPack['tags'] = tags

			formed_datapoints.append(dataPack)
		return formed_datapoints




if __name__ == '__main__':
	operator = InfluxOP(host='192.168.2.171', port=8086, username='zhijian', password='huizhijian')

	demo = [{'network_delay': 11, 'packet_loss': 0.02, 'sending_queue_delay': 340}]
	tags = {'version':'0.4'}



	# operator.pushData(database='rate_control',measurement = 'state_monitor', datapoints = demo, tags = tags )

	# results = operator.pullData(database='rate_control',measurement='state_monitor',tags=None, time_range='1d')


	# create database
	# results = operator.createDatabase(name='rate_control')


	# delete Date
	# results = operator.deleteData(database='Experiments',measurement = 'network_monitor', tags=None, time_before='5m')
	# print(results)

	# pull data
	# results = operator.pullData(database='Experiments',measurement='network_monitor',tags=None, time_range='5d')
	# print(results)

	# x = [data['time'] for data in results[0]]
	# y = [data['network_delay'] for data in results[0]]
	# print(x)
	# print(y)

	# # drop measurement
	# results = operator.dropMeasurement(database='rate_control',measurement='state_monitor')
	# print(results)

	# drop database
	results = operator.dropDatabase(database='Experiments')